package com.github.desafioandroid.extension

import android.app.Application
import com.github.dataLayer.di.DataLayerModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import com.github.desafioandroid.di.appModule

fun Application.startKoinApp() {
    startKoin {
        androidContext(this@startKoinApp)
        androidLogger()
        DataLayerModule.loadModules()
        loadKoinModules(appModule)
    }
}
