package com.github.desafioandroid.features.github.domain

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class License (
	@Json(name = "key") val key : String,
	@Json(name = "name") val name : String?,
	@Json(name = "spdx_id") val idSpdx : String?,
	@Json(name = "url") val url : String?,
	@Json(name = "node_id") val idNode : String?
): Parcelable