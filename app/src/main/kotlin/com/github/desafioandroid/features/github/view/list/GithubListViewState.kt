package com.github.desafioandroid.features.github.view.list

import com.github.desafioandroid.features.github.domain.Github

data class GithubListViewState(
    val github: Github
)