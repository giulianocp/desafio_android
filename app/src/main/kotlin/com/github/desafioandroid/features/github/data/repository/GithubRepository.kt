package com.github.desafioandroid.features.github.data.repository

import com.github.desafioandroid.features.github.data.api.GithubAPI
import com.github.desafioandroid.features.github.domain.Github

class GithubRepository(
    private val api: GithubAPI
) {
    suspend fun getRepors(): Github {
        return api.getRepors()
    }
}