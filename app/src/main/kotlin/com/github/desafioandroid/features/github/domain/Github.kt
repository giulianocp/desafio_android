package com.github.desafioandroid.features.github.domain

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Github (
	@Json(name = "total_count") val totalCount : Int = 0,
	@Json(name = "incomplete_results") val incompleteResults : Boolean = false,
	@Json(name = "items") val items : List<Items>?
): Parcelable