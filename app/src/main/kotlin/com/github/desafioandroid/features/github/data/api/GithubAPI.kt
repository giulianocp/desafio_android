package com.github.desafioandroid.features.github.data.api

import com.github.desafioandroid.features.github.domain.Github
import retrofit2.http.GET

interface GithubAPI {

    @GET("search/repositories?q=language:kotlin&sort=stars&page=1")
    suspend fun getRepors(): Github
}