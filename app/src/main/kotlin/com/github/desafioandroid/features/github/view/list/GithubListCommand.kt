package com.github.desafioandroid.features.github.view.list

sealed class GithubListCommand {
    object showLoading : GithubListCommand()
    object hideLoading : GithubListCommand()
}