package com.github.desafioandroid.features.github.view.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.dataLayer.data.remote.SafeResponse
import com.github.dataLayer.data.remote.safeRequest
import com.github.desafioandroid.features.github.domain.Github
import com.github.desafioandroid.features.github.domain.useCase.GetReporsUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class GithubListViewModel(
    private val getReporsUseCase: GetReporsUseCase,
    private val dispatcher: CoroutineContext = Dispatchers.Main + SupervisorJob()
) : ViewModel() {

    private val mutableViewState = MutableLiveData<GithubListViewState>()
    val viewState: LiveData<GithubListViewState> = mutableViewState

    private val mutableCommand = MutableLiveData<GithubListCommand>()
    val command: LiveData<GithubListCommand> = mutableCommand

    fun loadRepors() {
        viewModelScope.launch(dispatcher) {
            GithubListCommand.showLoading.run()

            when (val response = safeRequest { getReporsUseCase() }) {
                is SafeResponse.Success -> onSuccess(response.value)
                is SafeResponse.GenericError -> onGenericError()
                is SafeResponse.NetworkError -> onNetworkError()
            }

            GithubListCommand.hideLoading.run()
        }
    }

    private fun onSuccess(github: Github) {
        GithubListViewState(
            github = github
        ).run()
    }

    private fun onGenericError(){
        GithubListCommand.hideLoading.run()
    }

    private fun onNetworkError(){
        GithubListCommand.hideLoading.run()
    }

    private fun GithubListViewState.run() {
        mutableViewState.postValue(this)
    }

    private fun GithubListCommand.run() {
        mutableCommand.postValue(this)
    }

}
