package com.github.desafioandroid.features.github.view

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.github.desafioandroid.R

class GithubActivity : AppCompatActivity(R.layout.activity_github) {

    companion object {
        fun intent(context: Context): Intent {
            return Intent(context, GithubActivity::class.java)
        }
    }
}
