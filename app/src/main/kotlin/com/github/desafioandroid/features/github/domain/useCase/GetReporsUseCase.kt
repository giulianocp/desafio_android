package com.github.desafioandroid.features.github.domain.useCase

import com.github.desafioandroid.features.github.data.repository.GithubRepository
import com.github.desafioandroid.features.github.domain.Github

class GetReporsUseCase(private val repository: GithubRepository) {

    suspend operator fun invoke(): Github {
        return repository.getRepors()
    }

}