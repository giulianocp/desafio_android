package com.github.desafioandroid.features.github.view.list

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.desafioandroid.R
import com.github.desafioandroid.features.github.view.list.component.ReporsAdapter
import kotlinx.android.synthetic.main.fragment_github_list.*
import org.koin.android.viewmodel.ext.android.viewModel

class GitHubListFragment : Fragment(R.layout.fragment_github_list) {

    private val viewModel by viewModel<GithubListViewModel>()
    private val reporsAdapter by lazy { ReporsAdapter(requireContext()) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        setupClickListeners()
        viewModel.loadRepors()
    }

    private fun setupObservers() {
        viewModel.viewState.observe(viewLifecycleOwner, Observer { setupViewState(it) })
        viewModel.command.observe(viewLifecycleOwner, Observer {
            when (it) {
                is GithubListCommand.showLoading -> {
                    showLoading()
                }
                is GithubListCommand.hideLoading -> {
                    hideLoading()
                }
            }
        })
    }

    private fun setupViewState(viewState: GithubListViewState) {
        viewState.github.items?.let {
            reporsAdapter.repors = it.toMutableList()
            with(rvRepors) {
                adapter = reporsAdapter
                layoutManager =
                    LinearLayoutManager(
                        requireContext(),
                        LinearLayoutManager.VERTICAL,
                        false
                    )
            }
        }
    }

    private fun setupClickListeners() {
        reporsAdapter.onRepoClick = {
            val browseIntent = Intent(Intent.ACTION_VIEW, Uri.parse(it.htmlUrl))
            startActivity(browseIntent)
        }
    }

    private fun hideLoading() {
        progressBar.isVisible = false
        rvRepors.isVisible = true
    }

    private fun showLoading() {
        progressBar.isVisible = true
        rvRepors.isVisible = false
    }
}