package com.github.desafioandroid.features.github.view.list.component

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.github.desafioandroid.R
import com.github.desafioandroid.features.github.domain.Items
import kotlinx.android.synthetic.main.view_github_item.view.*

class ReporsAdapter(val context: Context) : RecyclerView.Adapter<ReporsAdapter.ViewModel>() {

    var onRepoClick: (Items) -> Unit = {}

    var repors: MutableList<Items> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewModel {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_github_item, parent, false)
        return ViewModel(view)
    }

    override fun getItemCount(): Int = repors.size

    override fun onBindViewHolder(holder: ViewModel, position: Int) {
        val repo = repors[position]

        with(holder.itemView) {
            Glide.with(this).load(repo.owner.avatarUrl).into(imgProfileOwner)
            txtTitleRepo.text = repo.fullName
            txtNameOwner.text = "@${repo.owner.login}"
            txtUrl.text = repo.cloneUrl
            txtDescription.text = repo.description
            forkCount.text = if (repo.forks > 0) repo.forks.toString() else "0"
            starCount.text = if (repo.stargazersCount > 0) repo.stargazersCount.toString() else "0"

            iconRight.setOnClickListener {
                onRepoClick(repo)
            }
        }
    }

    class ViewModel(view: View) : RecyclerView.ViewHolder(view)
}