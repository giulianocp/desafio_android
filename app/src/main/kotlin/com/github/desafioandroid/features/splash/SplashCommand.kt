package com.github.desafioandroid.features.splash

sealed class SplashCommand {
    object OpenHome : SplashCommand()
}