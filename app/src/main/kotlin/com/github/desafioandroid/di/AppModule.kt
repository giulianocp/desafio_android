package com.github.desafioandroid.di

import com.github.dataLayer.data.remote.HttpClient
import com.github.desafioandroid.features.github.data.api.GithubAPI
import com.github.desafioandroid.features.github.data.repository.GithubRepository
import com.github.desafioandroid.features.github.domain.useCase.GetReporsUseCase
import com.github.desafioandroid.features.github.view.list.GithubListViewModel
import com.github.desafioandroid.features.splash.SplashViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    viewModel { SplashViewModel() }
    viewModel { GithubListViewModel(get()) }

    factory { GetReporsUseCase(get()) }
    factory { GithubRepository(get()) }
    factory { get<HttpClient>().create(GithubAPI::class.java) }
}
