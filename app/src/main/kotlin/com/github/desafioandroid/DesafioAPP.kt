package com.github.desafioandroid

import com.github.desafioandroid.extension.startKoinApp

class DesafioAPP : android.app.Application() {

    override fun onCreate() {
        super.onCreate()
        startKoinApp()
    }

}